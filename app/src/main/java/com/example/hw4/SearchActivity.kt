package com.example.hw4

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteStatement
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.search_layout.*


class SearchActivity : AppCompatActivity() , View.OnClickListener{

    private var connectionToDatabase : SQLiteDatabase? = null

    var sqlStatement : SQLiteStatement? = null
    var resultSet : Cursor? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.search_layout)

        connectionToDatabase = this.openOrCreateDatabase ("employee.db",MODE_PRIVATE
            ,null)

        connectionToDatabase!!.execSQL ( "CREATE TABLE if not exists table1 ("
                + "name text not null, "
                + "id Long not null primary key,"
                + "salary Long ,"
                + "age integer,"
                + "job text );")


        send.setOnClickListener(this)
        back.setOnClickListener(this)
                                                       }//onCreate

    override fun onClick(v: View?) {


        if (v == send) {

            if (!idText.text.isEmpty()) {

                resultSet = connectionToDatabase!!.rawQuery(
                    "select * from table1 where id = " + idText.text
                    , null)

                if (resultSet != null && resultSet!!.count > 0) {

                    resultSet!!.moveToFirst()

                    IDText.text = resultSet!!.getString(1)
                    NameText.text = resultSet!!.getLong(0).toString()
                    SalaryText.text = resultSet!!.getLong(2).toString()
                    AgeText.text = resultSet!!.getInt(3).toString()
                    JobText.text = resultSet!!.getString(4)

                    IDText.visibility = View.VISIBLE
                    NameText.visibility = View.VISIBLE
                    SalaryText.visibility = View.VISIBLE
                    AgeText.visibility = View.VISIBLE
                    JobText.visibility = View.VISIBLE

                    Toast.makeText(this,"Results !",Toast.LENGTH_LONG).show()

                                                                 }//if statement

                else
                    Toast.makeText(
                        this, "This ID : " + idText.text + " does not exist in the database !"
                        , Toast.LENGTH_LONG).show()

                                         }//if statement

            else
                Toast.makeText(this, "Please fill out all fields !", Toast.LENGTH_LONG).show()


                        }//if statement

        else if (v == back)
            this.finish()
                                    }//onClick

                                                                      }//SearchActivity