package com.example.hw4

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteStatement
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.delete_layout.*


class DeleteActivity : AppCompatActivity(),View.OnClickListener {

    private var connectionToDatabase : SQLiteDatabase? = null

    var sqlStatement : SQLiteStatement? = null
    var resultSet : Cursor? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.delete_layout)

        connectionToDatabase = this.openOrCreateDatabase ("employee.db", MODE_PRIVATE
            ,null)

        connectionToDatabase!!.execSQL ( "CREATE TABLE if not exists table1 ("
                + "name text not null, "
                + "id Long not null primary key,"
                + "salary Long ,"
                + "age integer,"
                + "job text );")


        send.setOnClickListener(this)
        back.setOnClickListener(this)
                                                       }//onCreate

    override fun onClick(v: View?) {


        if (v == send) {

            if (!idText.text.isEmpty()) {

                resultSet = connectionToDatabase!!.rawQuery(
                    "select * from table1 where id = " + idText.text
                    , null)

                if (resultSet != null && resultSet!!.count > 0) {

                    connectionToDatabase!!.execSQL ( "delete from table1 where id  = " + idText.text )
                    Toast.makeText(this,"Record deleted successfully",Toast.LENGTH_LONG).show()

                                                                }//if statement

                else
                    Toast.makeText(
                        this, "This ID : " + idText.text + " does not exist in the database !"
                        , Toast.LENGTH_LONG).show()

                                        }//if statement

            else
                Toast.makeText(this, "Please enter ID that you want to delete !",
                    Toast.LENGTH_LONG).show()


                        }//if statement

        else if (v == back)
            this.finish()
                                     }//onClick

                                                                   }//DeleteActivity