package com.example.hw4

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() , View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        insertButton.setOnClickListener(this)
        deleteButton.setOnClickListener(this)
        editButton.setOnClickListener(this)
        searchButton.setOnClickListener(this)
        displayButton.setOnClickListener(this)

                                                       }//onCreate

    override fun onClick(v: View?) {

        when (v) {
            insertButton -> startActivity(Intent(applicationContext,AddActivity::class.java))
            deleteButton -> startActivity(Intent(applicationContext,DeleteActivity::class.java))
            editButton -> startActivity(Intent(applicationContext,EditActivity::class.java))
            searchButton -> startActivity(Intent(applicationContext,SearchActivity::class.java))
            displayButton -> startActivity(Intent(applicationContext,DisplayActivity::class.java))
                  }//when statement


                                   }//onClick

                                         }//MainActivity
