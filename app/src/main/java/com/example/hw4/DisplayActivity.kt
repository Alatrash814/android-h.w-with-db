package com.example.hw4

import android.app.Activity
import android.app.PendingIntent.getActivity
import android.content.Context
import android.database.Cursor
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import kotlinx.android.synthetic.main.display_layout.*
import kotlinx.android.synthetic.main.dit.view.*
import java.net.IDN
import java.util.ArrayList


class DisplayActivity : AppCompatActivity() {

    private var ListOfRecords = ArrayList<Record>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.display_layout)

        ListOfRecords = FindAllRecords()

        var Adapter : RecordAdapter = RecordAdapter (ListOfRecords, applicationContext)

        listOfRecords.adapter = Adapter

                                                       }//onCreate


    fun FindAllRecords () : ArrayList <Record> {

        var RecordList : ArrayList<Record> = ArrayList()


        var connectionToDatabase = this.openOrCreateDatabase ("employee.db",MODE_PRIVATE
            ,null)

        connectionToDatabase!!.execSQL ( "CREATE TABLE if not exists table1 ("
                + "name text not null, "
                + "id Long not null primary key,"
                + "salary Long ,"
                + "age integer,"
                + "job text );")

        var resultSet : Cursor = connectionToDatabase!!.rawQuery(
            "select * from table1 ", null)

        if (resultSet.count != 0) {

            var count = resultSet.count

            while (count > 0){

                resultSet.moveToNext()

                var temp : Record = Record(resultSet.getLong(1), resultSet.getString(0)
                                            ,resultSet.getLong(2),resultSet.getInt(3)
                                            ,resultSet.getString(4))
                RecordList.add(temp)
                count--
                             }//while loop



                                  }//if statement

            return RecordList
                                               }//FindAllRecords




    class RecordAdapter : BaseAdapter {


        var ListOfRecords : List<Record>? = null
        var context: Context? = null


        constructor(listofrecords : List<Record> , cont : Context){

            ListOfRecords = listofrecords
            context = cont

                                                                  }//constructor

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

            val record = ListOfRecords?.get(position)
            val inflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val MyView : View = inflater.inflate(R.layout.dit, null)
            MyView.ID.text = "ID : " + record!!.ID.toString()
            MyView.Name.text = "Name : " + record!!.Name
            MyView.Age.text = "Age : " + record!!.Age.toString()
            MyView.Salary.text = "Salary : " + record!!.Salary.toString()
            MyView.Job.text = "Job : " + record!!.Job.toString()



            return MyView
                                                                                          }//getView

        override fun getItem(position: Int): Any {
            return ListOfRecords!![position]
                                                 }//getItem

        override fun getItemId(position: Int): Long {
            return position as Long
                                                    }//getItemId

        override fun getCount(): Int {
            return ListOfRecords!!.size
                                     }//getCount

                                        }//RecordAdapter

                       }//DisplayActivity


