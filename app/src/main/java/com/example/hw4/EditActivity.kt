package com.example.hw4

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteStatement
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.edit_layout.*


class EditActivity : AppCompatActivity() , View.OnClickListener{

    private var connectionToDatabase : SQLiteDatabase? = null

    var sqlStatement : SQLiteStatement? = null
    var resultSet : Cursor? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_layout)

        connectionToDatabase = this.openOrCreateDatabase ("employee.db", MODE_PRIVATE
            ,null)

        connectionToDatabase!!.execSQL ( "CREATE TABLE if not exists table1 ("
                + "name text not null, "
                + "id Long not null primary key,"
                + "salary Long ,"
                + "age integer,"
                + "job text );")


        send.setOnClickListener(this)
        back.setOnClickListener(this)
                                                       }//onCreate

    override fun onClick(v: View?) {


        if (v == send) {

            if (!idText.text.isEmpty() && !nameText.text.isEmpty() && !ageText.text.isEmpty()
                && !jobText.text.isEmpty() && !salaryText.text.isEmpty()) {


                resultSet = connectionToDatabase!!.rawQuery(
                    "select * from table1 where id = " + idText.text
                    , null)

                if (resultSet != null && resultSet!!.count > 0) {



                    connectionToDatabase!!.execSQL ( "delete from table1 where id  = " + idText.text )

                    sqlStatement = connectionToDatabase!!.compileStatement(
                        "insert into table1 values(?,?,?,?,?)")

                    sqlStatement!!.bindString(1, nameText.text.toString())
                    sqlStatement!!.bindLong(2, java.lang.Long.parseLong(idText.text.toString()))
                    sqlStatement!!.bindLong(3, java.lang.Long.parseLong(salaryText.text.toString()))
                    sqlStatement!!.bindLong(4, java.lang.Long.parseLong(ageText.text.toString()))
                    sqlStatement!!.bindString(5, jobText.text.toString())
                    sqlStatement!!.executeInsert()

                    Toast.makeText(this,"Record updated successfully", Toast.LENGTH_LONG).show()

                                                                 }//if statement

                else
                    Toast.makeText(
                        this, "This ID : " + idText.text + " does not exist in the database !"
                        , Toast.LENGTH_LONG).show()

                                                                            }//if statement

            else
                Toast.makeText(this, "Please fill out all fields !", Toast.LENGTH_LONG).show()


                     }//if statement

        else if (v == back)
            this.finish()
                                   }//onClick

                                                                   }//EditActivity